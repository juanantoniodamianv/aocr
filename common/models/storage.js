'use strict';

const fileImporter = require('../../server/lib/fileImporter'),
      group_container = 'groups',
      createGroup = require('../../server/lib/medicus/createGroup')

function createError(message, status = 422) {
  var error = new Error(message);
  error.statusCode = status
  return error
}

module.exports = function(Storage) {

  Storage.afterRemote('upload', async (ctx, res) => {
    var file = res.result.files.file[0]

    /**
     * Create group after upload CSV for Insured Groups
     * @param {object} Storage 
     * @param {object} res 
     */
    let createGroupAOCR = async (Storage, res) => {
      let Group = Storage.app.models.Group

      let group_name = res.result.fields.groupName[0]
      let valid_until = res.result.fields.validUntil[0]
      let company_id = res.result.fields.insuranceCompany[0]
      let policy_number = res.result.fields.policyNumber[0]

      if (group_name) {
        try {
          /* Create new Group into Medicus API and return GroupId */
          let medicusGroupId = await createGroup(group_name);
          let group = await Group.create({
            name: group_name,
            validUntil: valid_until,
            policyNumber: policy_number,
            companyId: company_id,
            medicusGroupId
          })
          return { groupId: group.id, medicusGroupId }
        } catch (e) {
          console.log(`Error: ${e}`)
        }

      } else { 
        return createError('Group name (groupName) field is required.')
      }
    }
    
    let importedRows

    /* Only for Insured Groups */
    try {
      if (file.container == group_container) {
        let newGroup = await createGroupAOCR(Storage, res)
        importedRows = await fileImporter(Storage, file, newGroup)
      } else {
        importedRows = await fileImporter(Storage, file, { groupId: null, medicusGroupId: null })
      }
    } catch (e) {
      throw e
    }

    ctx.result.importedRows = importedRows

  });

};

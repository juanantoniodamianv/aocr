"use strict"


let db = {
  connector: process.env.DB_CONNECTOR,
  file: process.env.DB_FILE,
}
if (process.env.DB_CONNECTOR !== 'memory') {
  db = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    user: process.env.DB_USER,
    connector: process.env.DB_CONNECTOR,
  }
}

module.exports = {
  db: db,
}

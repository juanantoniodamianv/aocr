'use strict';

const request = require('request'),
      initialize = require('./auth');

/**
 * Adds a number of subscribers to a group (with names/emails/or anonymous)
 * @param {string} access_token 
 * @param {array} subscribers [{name, email},{...}]
 */

let addSubscribers = (access_token, subscribers, medicusGroupId) => {
  console.log(`GROUP ID: ${medicusGroupId}`)
  medicusGroupId = medicusGroupId == null ? '1' : medicusGroupId
  let authorization = `Bearer ${access_token}`;
  var options = {
    url: process.env.MEDICUS_ADD_SUBSCRIBER,
    headers: { 
      'content-type': 'application/json',
      'Authorization': authorization
    },
    body: JSON.stringify({
      'subscribers': subscribers,
      'group_id': medicusGroupId
    })
  };

  return new Promise((resolve, reject) => {
    request.post(options, (err, resp, body) => {
      if (err) {
        reject(err)
      } else {
        let bodyParser = JSON.parse(body)
        resolve(bodyParser.data.data)
      }
    })
  })
}

module.exports = async (subscribers, medicusGroupId) => {
  try {
    let access_token = await initialize();
    let newSubscribers = await addSubscribers(access_token, subscribers, medicusGroupId)
    
    return newSubscribers
  } catch (e) {
    console.log(`Error: ${e}`)
  }

};
'use strict';

var request = require("request");

/**
 * Initialize connection with Medicus API
 * @param {string} grant_type password / refresh_token
 * @param {string} username
 * @param {string} password
 * @param {string} client_id
 * @param {string} client_secret
 */

module.exports = () => {
  var options = {
    url: process.env.MEDICUS_URI_AUTH, 
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      'grant_type': process.env.GRANT_TYPE,
      'username': process.env.USERNAME,
      'password': process.env.PASSWORD,
      'client_id': process.env.CLIENT_ID,
      'client_secret': process.env.CLIENT_SECRET
    })
  };

  return new Promise((resolve, reject) => {
    request.post(options, (err, resp, body) => {
      if (err) {
        reject(err)
      } else {
        let bodyParser = JSON.parse(body)

        resolve(bodyParser.access_token)
      }
    })
  })
}
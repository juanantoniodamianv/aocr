'use strict';

const request = require('request'),
      initialize = require('./auth');

let createGroup = (access_token, groupName) => {
  console.log(`Group Name: ${groupName}`)
  let authorization = `Bearer ${access_token}`;
  var options = {
    url: process.env.MEDICUS_CREATE_GROUP,
    headers: { 
      'content-type': 'application/json',
      'Authorization': authorization
    },
    body: JSON.stringify({
      'name': groupName.toString()
    })
  };

  return new Promise((resolve, reject) => {
    request.post(options, (err, resp, body) => {
      if (err) {
        reject(err)
      } else {
        let bodyParser = JSON.parse(body)
        resolve(bodyParser.data.group.id)
      }
    })
  })
};

/**
 * Initialize Medicus API and Create Group
 * @param {string} groupName
 */

module.exports = async (groupName) => {
  try {
    let access_token = await initialize();
    let newGroup = await createGroup(access_token, groupName);
    
    return newGroup
  } catch (e) {
    console.log(`Error: ${e}`)
  }

};
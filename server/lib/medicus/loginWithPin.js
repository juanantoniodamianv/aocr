'use strict'

const request = require('request'),
      initialize = require('./auth');

let loginWithPin = (access_token, pin) => {
  let authorization = `Bearer ${access_token}`;
  var options = {
    url: process.env.MEDICUS_LOGIN_WITH_PIN,
    headers: {
      'content-type': 'application/json',
      'Authorization': authorization
    },
    body: JSON.stringify({
      'pin': pin
    })
  };

  return new Promise((resolve, reject) => {
    request.post(options, (err, resp, body) => {
      if (err) {
        reject(err)
      } else {
        let bodyParser = JSON.parse(body)
        resolve(bodyParser.data.subscriber)
      }
    })
  })
};

/**
 * Initialize Medicus API and Login Subscriber (and activate his code PIN if not activated yet)
 * @param {number} pin Subscriber PIN
 */

 module.exports = async (pin) => {
   try {
     let access_token = await initialize();
     let subscriberLogin = await loginWithPin(access_token, pin);

     return subscriberLogin
   } catch (e) {
     console.log(`Error: ${e}`)
   }
 };
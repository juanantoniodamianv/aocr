'use strict';

const path = require('path'),
      parseCsv = require('csv-parse'),
      fs = require('fs'),
      container = 'insureds',
      groupContainer = 'groups',
      today = new Date(),
      addSubscribers = require('./medicus/addSubscribers')

/* Rewards Program */
let insured_columns = [
  { 'name': 'dealId', 'title': 'Deal - ID' },
  { 'name': 'dealTitle', 'title': 'Deal - Title' },
  { 'name': 'dealPersonId', 'title': 'Person - ID' },
  { 'name': 'contactPerson', 'title': 'Deal - Contact person' },
  { 'name': 'email', 'title': 'Person - Email' },
  { 'name': 'dealInsurer', 'title': 'Deal - Insurer' }, /* This field is in Company model */
]

/* Insured Groups */
let group_columns = [
  { 'name': 'firstName', 'title': 'First Name' },
  { 'name': 'lastName', 'title': 'Last Name' },
  { 'name': 'relationship', 'title': 'Relationship' },
  { 'name': 'email', 'title': 'Email' }
]

let createError = (message, status = 422) => {
  var error = new Error(message);
  error.statusCode = status
  return error
}

let parseFile = (file) => {
  let filePath = path.join(file.client.root, file.container, file.name);

  if (filePath.substr(-4) == '.csv') {
    return new Promise((resolve, reject) => {
      
      fs.readFile(filePath, (err, data) => {
        if (err) {
          return reject(createError('Cannot read CSV file'))
        }
        let options = {
          columns: (file.container == container) ? insured_columns.map(c => c.name) : group_columns.map(c => c.name),
          delimiter: ','
        }
        parseCsv(data, options, (err, rows) => {
          if (err) {
            return reject(createError('Cannot parse CSV file (check if the file is a valid CSV and if the rows match the expected format)'))
          }
          resolve(rows)
        })
      })
    })
  }
  throw createError('Only CSV file can be imported')
}

module.exports = async (Storage, file, group) => {
  const { groupId, medicusGroupId } = group

  /* Group and Insured containers is only accepted */
  if (file.container != container && file.container != groupContainer) return

  /* If Group container if selected, group parameter must been exist */
  if (file.container == groupContainer && groupId == null) return
 

  let loadFile = () => new Promise(function (resolve, reject) {
    Storage.app.dataSources.storage.connector.getFile(file.container, file.name, (err, file) => {
      if (err) {
        reject(createError('Cannot find the file or the storage'))
      }
      resolve(file)
    })
  })

  let content = await loadFile()
  let rows = await parseFile(content)
  
  rows.shift()
  
  let Insured = Storage.app.models.Insured
  let CompanyInsured = Storage.app.models.CompanyInsured
  let Company = Storage.app.models.Company
  let subscribers = []
  let email
  
  /* If container is Insureds */
  if (file.container == container) {
    for (let row of rows) {
      /* Deal Title year value must be equal to the current year. Person Id, Deal Contact Person and Person Email can't be blank */
      if (row.dealTitle.substr(-4) != today.getFullYear() || row.dealPersonId == '' || row.contactPerson == '' || row.email == '') continue;

      email = row.email.split(", ")[0] 

      let insured = await Insured.findOne({
        where: {
          email: email
        }
      })

      if (!insured) {
        insured = await Insured.create({
          contactPerson: row.contactPerson,
          email: email,
          dealId: row.dealId,
          dealTitle: row.dealTitle,
          dealPersonId: row.dealPersonId,
          aocPoints: 500
        })

        /* Create subscribers name and email to create New Subscribers into Medicus API */
        if (email != undefined) {
          subscribers = [...subscribers, { "name": row.contactPerson, "email": email }]
        }
      } else {
        insured = await insured.updateAttributes({
          contactPerson: row.contactPerson,
          dealId: row.dealId,
          dealTitle: row.dealTitle
        })
      }

      //  Relationship with Company and Insured
      if (row.dealInsurer != '') {        
        let company = await Company.findOne({
          where: {
            name: row.dealInsurer
          }
        })
        if (company) {
          let company_insured = await CompanyInsured.findOne({
            where: {
              companyId: company.id,
              insuredId: insured.id
            }
          })
          if (!company_insured) {
            await CompanyInsured.create({
              companyId: company.id,
              insuredId: insured.id
            })
          }
        } else {
          console.log(`Warning: Contact Deal N°: ${row.dealPersonId}, wich Deal Insurer: ${row.dealInsurer} doesn't exist in AOCR database.`)
        }
      } 

    }
  } else if (file.container == groupContainer) { /* If container is Groups */
    for (let row of rows) {
      /* Email can't be blank */
      if (row.email == '') continue;

      let fullName = row.firstName + ' ' + row.lastName

      let insured = await Insured.findOne({
        where: {
          email: row.email
        }
      })

      if (!insured) {
        insured = await Insured.create({
          contactPerson: fullName,
          relationship: row.relationship,
          email: row.email,
          aocPoints: 500,
          groupId
        })
        /* Create new subscribers into Medicus API with email and name */
        email = row.email.split(", ")[0] 
        if (email != undefined) {
          subscribers = [...subscribers, { "name": fullName, "email": email }] 
        }
      }

    }
  }

  console.log(`Subscribers quantity: ${subscribers.length}`)
  console.log(subscribers)
  if (subscribers.length > 0) {
    try {
      //let newSubscribers = await addSubscribers(subscribers, medicusGroupId);
      let newSubscribers = 0
      console.log(`Creating new subscribers from file importer...`)
      /* Update Insureds model with new Medicus subscribers */
      if (newSubscribers.length > 0) {
        for (let newSubscriber of newSubscribers) {
          console.log('-------------------------------------------------------------------------')
          console.log(`Updating Insured... (${newSubscriber.email} - ${newSubscriber.pin})`)
          let ins = await Insured.findOne({
                      where: {
                        email: newSubscriber.email
                      }
                    });
          if (ins) {
            await ins.updateAttributes({
              medicusPin: newSubscriber.pin
            })
          }
        } 
      }
    } catch (e) {
      throw e
    }
  }
  
  return

}

"use strict"

module.exports = async function (app) {
  const Storage = app.models.Storage

  let containers = [
    "insureds",
    "groups"
  ]

  Storage.getContainers((err, existing) => {
    for (let container of containers) {
      if (!existing.some((ex) => ex.name == container)) {
        Storage.createContainer({
          name: container
        }, (err) => {})
      }
    }
  })

  const Company = app.models.Company

  if (await Company.count() == 0) {
    let companies = [
      "A+ International Healthcare",
      "Aetna",
      "ALC Global Health Insurance",
      "Allianz Care",
      "April International",
      "April Magellan",
      "AXA PPP",
      "BUPA Global",
      "CIGNA Global Health",
      "David Shield",
      "Foyer Global Health",
      "Generali Equite",
      "Globality Health",
      "HealthCare International",
      "Henner CFE",
      "Henner Expat",
      "Humanis",
      "IMG",
      "Indigo Expat",
      "IntegraGlobal",
      "IPH Insurance",
      "Morgan Price",
      "MSH International",
      "Now Health (SimpleCare)",
      "Now Health (WorldCare)",
      "Safe Meridian",
      "Swiss Global Health Insurance",
      "Swiss Life",
      "Wellaway",
      "William Russell"
    ]
    
    companies.forEach(async (company) => {
      console.log(`Creating company ${company}`)
      await Company.create({ name: company })
    })
  }

  
}
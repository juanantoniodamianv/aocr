/* 'use strict'

module.exports = function(app) {
 
 if(process.env.SKIP_WORKER == 1) {
   console.log('Will skip the background worker')
   return
  } else {
    const Queue = require('bull'),
          updateInsured = new Queue('Update insured according to Medicus API', process.env.REDIS_URL),
          loginWithPin = require('../lib/medicus/loginWithPin'),
          Insured = app.models.Insured;
  
    updateInsured.process(async (job, done) => {
      
      console.log(`\n[Bull] Launched: ${Date(Date.now()).toString()}`)
      try {
        if (await Insured.count() == 0) {
          console.log(`[Bull] Nothing to worker yet :)\n`)
        } else {
          console.log(`[Bull] Updating Insureds...`)
          let insureds = await Insured.find()
      
          if (insureds) {
            for (let insured of insureds) {
              if (insured.medicusPin == '') continue
              let subscriber = await loginWithPin(insured.medicusPin)
              // Is valid Subscriber?
              if (subscriber.points != undefined) {
                await insured.updateAttributes({
                  medicusPoints: subscriber.points,
                  medicusStage: subscriber.stage
                })
              }
            }
          }
          console.log(`[Bull] Insureds are updated successfully.\n`)
        }
      } catch(err) {
        console.log(new Error(`[Bull error]: ${err}`))
      }

      done();
    })
    
    
    updateInsured.add({data: 'any'},{ 
      repeat: {
        cron: '0 0 *1 * * ?' 
      }  
    }); 
  }

}; */
